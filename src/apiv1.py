from sanic import Blueprint, response
from sanic.response import json
import logging
from outputwriter import AsyncBufferedWriter, AsyncBufferedFormatter
import aiohttp
import urllib.parse

from statistics import AsyncStatistics
import math
from collections import Counter, OrderedDict
import json as jsn
from datetime import date

logger = logging.getLogger(__name__)

apiv1 = Blueprint("api_v1", version=1, version_prefix="/api/v")

# Placeholder for the connection pool
apiv1.ctx.statistics = None
apiv1.ctx.upl_data = None
apiv1.ctx.upl_date = None


@apiv1.before_server_start
async def setup_db_pool(app, loop):
    apiv1.ctx.statistics = AsyncStatistics.get_instance()
    await apiv1.ctx.statistics.open()



@apiv1.after_server_stop
async def close_db_pool(app, loop):
    if apiv1.ctx.statistics:
        await apiv1.ctx.statistics.close()
        apiv1.ctx.statistics = None

class AsyncBufferedSanicWriter(AsyncBufferedWriter):
    def __init__(self,request):
        super().__init__()
        self.request = request

    async def open(self, content_type):
        if content_type.startswith('text/'):
            content_type += "; charset=UTF-8"
        self.response = await self.request.respond(
            content_type=content_type, headers={"Content-Disposition": "inline"}
        )

    async def write(self,data):
        await self.response.send(data)

    async def close(self):
        await self.response.send(None)

async def _write_output(data, request=None, chunk_size=32, content_type=None):
    if not content_type:
        accept_header = request.headers.get("accept", "*/*")
        accepted = [h.split(";")[0] for h in accept_header.split(",")]
        supported_types = ["application/json", "application/x-ndjson", "text/csv"]
        logger.debug(f"Requested types: {accepted}")
        for t in accepted:
            if t in supported_types:
                content_type = t
                break
        if not content_type and "*/*" in accepted:
            content_type = "application/json"
        if not content_type:
            await request.respond(
                status=400,
                response=response.text(
                    f"no supported content-type provided in the accept header, only types {supported_types} accepted",
                    status=400,
                ),
            )
            return
    writer = AsyncBufferedSanicWriter(request)
    formatter = AsyncBufferedFormatter(writer,content_type)
    if not await formatter.write(data):
        logger.debug("no buffer at cleanup, no result, send 404")
        await request.respond(status=404)


EVALUATION_COUNT_FIELDS = [
    "is_evaluated",
    "num_correct_answers",
    "num_correct_answers_after_one_correction",
    "num_correct_answers_after_two_corrections",
    "num_failed_corrections",
    "num_incorrect_responses",
    "num_impossible_questions",
]

EVALUATION_BOOLEAN_FIELDS = [
    "citizen_properly_assisted",  # TODO this field is a NullBoolean, will probably change in the future
    "critical_error",
    "different_language",
    "personal_question",
]

FIELD_INFO = {
    "id": {"aggregate": None, "groupable": False, "filter": False},
    "session_id": {"aggregate": "count", "groupable": "true", "filter": True},
    "start": {"aggregate": None, "groupable": False, "filter": True},
    "end": {"aggregate": None, "groupable": False, "filter": True},
    "source": {"aggregate": None, "groupable": True, "filter": True},
    "escalate": {"aggregate": "percentage", "groupable": True, "filter": True},
    "escalate_result": {"aggregate": None, "groupable": True, "filter": True},
    "feedback": {"aggregate": None, "groupable": True, "filter": True},
    "feedback_result": {"aggregate": None, "groupable": True, "filter": True},
    "upl": {"aggregate": "percentage_list", "groupable": False, "filter": False},
    "performance": {"aggregate": "performance", "groupable": False, "filter": False},
    "organisation_id": {"aggregate": None, "groupable": True, "filter": True},
    "date": {"aggregate": None, "groupable": True, "filter": True},
    "hour": {"aggregate": None, "groupable": True, "filter": True},
    "iso_weekdate_year": {"aggregate": None, "groupable": True, "filter": True},
    "month": {"aggregate": None, "groupable": True, "filter": True},
    "week": {"aggregate": None, "groupable": True, "filter": True},
    "weekday": {"aggregate": None, "groupable": True, "filter": True},
    "year": {"aggregate": None, "groupable": True, "filter": True},
    "escalate_wait": {"aggregate": "wait", "groupable": False, "filter": False},
    "is_evaluated": {"aggregate": "sum_bool", "groupable": True, "filter": True},
    "subjects": {"aggregate": "percentage_list", "groupable": False, "filter": False},
    "citizen_properly_assisted": {
        "aggregate": "sum_bool",
        "groupable": True,
        "filter": True,
    },
    "critical_error": {"aggregate": "sum_bool", "groupable": True, "filter": True},
    "different_language": {"aggregate": "sum_bool", "groupable": True, "filter": True},
    "personal_question": {"aggregate": "sum_bool", "groupable": True, "filter": True},
    "num_correct_answers": {"aggregate": "sum", "groupable": True, "filter": True},
    "num_correct_answers_after_one_correction": {
        "aggregate": "sum",
        "groupable": True,
        "filter": True,
    },
    "num_correct_answers_after_two_corrections": {
        "aggregate": "sum",
        "groupable": True,
        "filter": True,
    },
    "num_failed_corrections": {"aggregate": "sum", "groupable": True, "filter": True},
    "num_incorrect_responses": {"aggregate": "sum", "groupable": True, "filter": True},
    "num_impossible_questions": {"aggregate": "sum", "groupable": True, "filter": True},
}


def custom_sort(item):
    if item[0] is False:
        return (0, item[1:], item[0])
    elif item[0] is True:
        return (1, item[1:], item[0])
    else:
        return (2, item[1:], item[0])


async def aggregate(generator, group_by_x=["single"], group_by_y=["single"]):
    if not group_by_y:
        group_by_y.append("single")
    if not group_by_x:
        group_by_x.append("single")

    tempdict = {}
    templist = []
    yvalues = []
    xvalues = []

    # conversation = await cursor.fetchone()
    # while conversation:
    async for conversation in generator:
        # logger.info(f'current record: {conversation}')
        xkeys = []
        ykeys = []
        for k in group_by_x:
            # FIX FOR GROUP ON UPL
            kv = conversation.get(k, "single")
            if type(kv) is list:
                # logger.info("list groupkey: {}".format(kv))
                if len(kv):
                    kv = list(set(kv))
                    kv.sort()
                    kv = ",".join(kv)
                    # logger.info("list groupkey unique: {}".format(kv))
                else:
                    kv = ""
            xkeys.append(kv)
        for k in group_by_y:
            # FIX FOR GROUP ON UPL
            kv = conversation.get(k, "single")
            if type(kv) is list:
                # logger.info("list groupkey: {}".format(kv))
                if len(kv):
                    kv = list(set(kv))
                    kv.sort()
                    kv = ",".join(kv)
                    # logger.info("list groupkey unique: {}".format(kv))
                else:
                    kv = ""
            ykeys.append(kv)
        tx = tuple(xkeys)
        ty = tuple(ykeys)
        if tx not in xvalues:
            xvalues.append(tx)
        if ty not in yvalues:
            yvalues.append(ty)
        ydict = tempdict.get(ty, {})
        tempdict[ty] = ydict
        gi = ydict.get(
            tx,
            {
                **{
                    "performance": {},
                    "count": 0,
                    "upl": [],
                    "subjects": [],
                    "escalate_wait": {},
                },
                **{
                    field: 0
                    for field in EVALUATION_COUNT_FIELDS + EVALUATION_BOOLEAN_FIELDS
                },
            },
        )
        total = 0
        for key, value in conversation["performance"].items():
            gi["performance"][key] = gi["performance"].get(key, 0) + value
            if key != "total":
                total += value
        if "total" not in conversation:
            gi["performance"]["total"] = gi["performance"].get("total", 0) + total
        wc = math.ceil((conversation["escalate_wait"] or 0) / 5) * 5
        if wc:
            gi["escalate_wait"][wc] = gi["escalate_wait"].get(wc, 0) + 1
        gi["upl"].extend(conversation["upl"])
        gi["count"] = gi["count"] + 1
        if conversation["is_evaluated"]:
            for field in EVALUATION_COUNT_FIELDS:
                gi[field] = gi[field] + (conversation.get(field, None) or 0)

            for field in EVALUATION_BOOLEAN_FIELDS:
                gi[field] = gi[field] + int(conversation.get(field, None) or 0)

            gi["subjects"].extend(conversation["subjects"])
        tempdict[ty][tx] = gi

        # conversation = await cursor.fetchone()

    yvalues = sorted(yvalues, key=custom_sort)
    xvalues = sorted(xvalues)

    data = {}
    for k, v in tempdict.items():
        i = OrderedDict()
        i["data"] = []
        i["performance"] = []
        for field in EVALUATION_COUNT_FIELDS + EVALUATION_BOOLEAN_FIELDS:
            i[field] = []
        i["upl"] = []
        i["subjects"] = []
        i["escalate_wait"] = []
        for x in xvalues:
            d = v.get(
                x,
                {
                    **{
                        "performance": {},
                        "count": 0,
                        "upl": [],
                        "subjects": [],
                        "escalate_wait": {},
                        "num_correct_answers": 0,
                    },
                    **{
                        field: 0
                        for field in EVALUATION_COUNT_FIELDS + EVALUATION_BOOLEAN_FIELDS
                    },
                },
            )
            i["data"].append(d["count"])
            for field in EVALUATION_COUNT_FIELDS + EVALUATION_BOOLEAN_FIELDS:
                i[field].append(d[field])
            i["escalate_wait"].append(d["escalate_wait"])
            p = {}
            total = d["performance"].get("total", None)
            if total:
                for key, value in d["performance"].items():
                    p[key] = value / total * 100
            i["performance"].append(p)

            for field in ["upl", "subjects"]:
                u = Counter(d[field])
                total = len(d[field])
                if total == 0:
                    total: None
                if total:
                    u["total"] = total
                    for key, value in u.items():
                        u[key] = value / total * 100
                    u = dict(sorted(u.items(), reverse=True, key=lambda item: item[1]))
                i[field].append(u)

        templist.append(i)
        labels = []
        for xval in xvalues:
            lbl = ""
            for idx, x in enumerate(xval):
                lbl = lbl + str(x)
                if idx < len(xval) - 1:
                    lbl = lbl + ","
            labels.append(lbl)

        for idx, x in enumerate(k):
            i[group_by_y[idx]] = x

        data["labels"] = labels
        data["datasets"] = templist
    return data

async def _list_statistics(request, organisation_id=None, session_id=None):
    """Example endpoint to fetch data from the database."""

    exclude_keys = {"groupByX", "groupByY"}

    # Build the dictionary
    params = {
        key: values if key == "session_id" else values[0]
        for key, values in request.args.items()
        if key not in exclude_keys
    }

    # process filter
    if organisation_id:
        params["organisation_id"] = organisation_id
    if session_id:
        params["session_id"] = session_id
        params = {
            key: value
            for key, value in params.items()
            if key in ["organisation_id", "session_id"]
        }
    logger.debug(f"params: {params}")

    async with apiv1.ctx.statistics() as executor:
        generator = await executor.query_statistics(**params)
        await _write_output(generator,request)

async def _get_aggregated(request, organisation_id=None):
    exclude_keys = {"groupByX", "groupByY"}

    # Build the dictionary
    params = {
        key: values[0]
        for key, values in request.args.items()
        if key not in exclude_keys
    }
    if organisation_id:
        params["organisation_id"] = organisation_id

    groupByX = [
        item
        for sublist in request.args.getlist("groupByX", [])
        for item in sublist.split(",")
    ]  # support both multiple arguments and single argument seperated by ,
    groupByY = [
        item
        for sublist in request.args.getlist("groupByY", [])
        for item in sublist.split(",")
    ]

    async with apiv1.ctx.statistics() as executor:
        generator = await executor.query_statistics(**params)
        result = await aggregate(generator, groupByX, groupByY)
        return json(result)

@apiv1.get("/aggregated/<organisation>")
# @protected ,token
async def aggregated_by_org(request, organisation):
    organisation = urllib.parse.unquote(organisation)
    return await _get_aggregated(request, organisation_id=organisation)


@apiv1.get("/aggregated")
async def aggregated(request):
    return await _get_aggregated(request)


@apiv1.get("/statistics")
# @protected ,token
async def get_statistics(request):
    await _list_statistics(request)


@apiv1.get("/statistics/<organisation>")
# @protected ,token
async def get_statistics_by_org(request, organisation):
    organisation = urllib.parse.unquote(organisation)
    await _list_statistics(request, organisation_id=organisation)


@apiv1.get("/statistics/<organisation>/<session_id>")
# @protected ,token
async def get_statistic(request, organisation, session_id):
    organisation = urllib.parse.unquote(organisation)
    await _list_statistics(request, organisation_id=organisation, session_id=session_id)


@apiv1.patch("/statistics/<organisation>/<session_id>")
# @protected ,token
async def update_statistic(request, organisation, session_id):
    organisation = urllib.parse.unquote(organisation)
    allowed_keys = [
        "subjects",
        "is_evaluated",
        "num_correct_answers",
        "num_correct_answers_after_one_correction",
        "num_correct_answers_after_two_corrections",
        "num_failed_corrections",
        "num_incorrect_responses",
        "num_impossible_questions",
        "citizen_properly_assisted",
        "critical_error",
        "different_language",
        "personal_question",
        "classification",
        "remarks",
    ]

    params = {"organisation_id": organisation, "session_id": session_id}

    async with apiv1.ctx.statistics() as executor:
        generator = await executor.query_statistics(**params)
        async for _ in generator:
            pass  # Iterate through all items without doing anything

        if not generator:
            await request.respond(status=404)
        else:
            data = request.json
            filtered_data = {
                key: data[key] for key in allowed_keys if key in data
            }
            if not filtered_data:
                await request.respond(status=400)
            await executor.update_statistics(organisation, session_id, **filtered_data)

    return json({"result": "success"})


async def _load_upl_subjects():
    today = date.today()
    if not apiv1.ctx.upl_date or today > apiv1.ctx.upl_date:
        logger.debug("load UPL data from server")
        UPL_URL = "https://standaarden.overheid.nl/owms/oquery/UPL-gemeente.json"
        """Fetch JSON data from a given URL asynchronously."""
        async with aiohttp.ClientSession() as session:
            async with session.get(UPL_URL) as response:
                if response.status == 200:
                    raw = await response.json()
                    result = []
                    bindings = raw["results"]["bindings"]
                    for item in bindings:
                        condition = True
                        # condition = "Burger" in item
                        if condition:
                            value = item["UniformeProductnaam"]["value"]
                            if value not in result:
                                result.append(value)

                    apiv1.ctx.upl_data = result
                    apiv1.ctx.upl_date = today
                else:
                    raise Exception(
                        f"Failed to fetch {UPL_URL}: HTTP {response.status}"
                    )
    else:
        logger.debug("load UPL data from cache")

    return apiv1.ctx.upl_data


async def _upl_generator():
    result = await _load_upl_subjects()
    for r in result:
        yield {"name": r}


@apiv1.route("/subjects/<organisation>")
async def subjects(request, organisation):
    organisation = urllib.parse.unquote(organisation)
    # organisation does not matter right now, for now, return UPL on all organisations
    await _write_output(_upl_generator(), request)

    return

@apiv1.route("/logs")
async def logs(request):
    # Build the dictionary
    # Retrieve a single parameter value (e.g., 'q')
    query = request.args.get('query', None)
    async with apiv1.ctx.statistics() as executor:
        generator = await executor.query_log(query)
        await _write_output(generator,request,content_type="text/plain")
