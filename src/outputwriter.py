import json
import logging
import os
from collections import Counter, OrderedDict
from datetime import datetime, date, timezone
from statistics import AsyncStatistics
from types import AsyncGeneratorType, SimpleNamespace
import csv
import json as jsn
from abc import ABC, abstractmethod

logger = logging.getLogger(__name__)

class BufferWriter:
    def __init__(self, ctx):
        self.ctx = ctx

    def write(self, data):
        logger.debug(f"write csv data: {data}")
        self.ctx.buffer += data.encode()


def cast_nullboolean_to_int(boolean):
    if boolean is None:
        return boolean

    return int(boolean)


def prepare_for_csv(data):
    performance_keys = [
        "total",
        "unsupported_language",
        "unsupported",
        "misunderstanding",
        "reparation",
        "escalate",
        "indirect",
        "ask",
        "chitchat",
        "direct",
    ]

    upl_columns = ["upl-" + str(i) for i in range(1, 6)]
    subjects_columns = ["subjects-" + str(i) for i in range(1, 6)]
    performance_columns = ["performance-" + key for key in performance_keys]

    columns = [
        "start",
        "end",
        "session_id",
        "source",
        "escalate",
        "escalate_result",
        "escalate_wait",
        "feedback",
        "feedback_result",
        "upl",
        *upl_columns,
        "performance",
        *performance_columns,
        "is_evaluated",
        "subjects",
        *subjects_columns,
        "num_correct_answers",
        "num_correct_answers_after_one_correction",
        "num_correct_answers_after_two_corrections",
        "num_failed_corrections",
        "num_incorrect_responses",
        "num_impossible_questions",
        "citizen_properly_assisted",
        "critical_error",
        "different_language",
        "personal_question",
        "classification",
        "remarks",
    ]

    if not data:
        return columns

    statistic = SimpleNamespace(**data)

    # upl = [item[0] for item in Counter(statistic.upl).most_common()]
    citizen_properly_assisted = statistic.citizen_properly_assisted

    if citizen_properly_assisted is None:
        if statistic.is_evaluated:
            citizen_properly_assisted = "Impossible"
        else:
            citizen_properly_assisted = ""

    return [
        statistic.start.strftime("%d-%m-%Y %H:%M:%S"),
        statistic.end.strftime("%d-%m-%Y %H:%M:%S"),
        statistic.session_id,
        statistic.source,
        statistic.escalate,
        statistic.escalate_result,
        getattr(statistic, "escalate_wait", ""),
        statistic.feedback,
        statistic.feedback_result,
        statistic.upl,
        *[
            statistic.upl[i] if i < len(statistic.upl) else ""
            for i in range(len(upl_columns))
        ],
        statistic.performance,
        *[statistic.performance.get(i, 0) for i in performance_keys],
        cast_nullboolean_to_int(statistic.is_evaluated),
        statistic.subjects,
        *[
            statistic.subjects[i] if i < len(statistic.subjects) else ""
            for i in range(len(subjects_columns))
        ],
        statistic.num_correct_answers,
        statistic.num_correct_answers_after_one_correction,
        statistic.num_correct_answers_after_two_corrections,
        statistic.num_failed_corrections,
        statistic.num_incorrect_responses,
        statistic.num_impossible_questions,
        citizen_properly_assisted,
        cast_nullboolean_to_int(statistic.critical_error),
        cast_nullboolean_to_int(statistic.different_language),
        cast_nullboolean_to_int(statistic.personal_question),
        statistic.classification,
        statistic.remarks,
    ]

class AsyncBufferedWriter(ABC):
    async def open(self, content_type):
        pass

    async def write(self,data):
        pass

    async def close(self,data):
        pass

class AsyncBufferedFormatter():
    def __init__(self,writer,content_type='application/x-ndjson',chunk_size=32):
        self.chunk_size = chunk_size
        self.writer = writer
        self.content_type = content_type

    async def _write_record(self,data):
        if data is None:
            logger.debug("data is none, finish up")
            if self.content_type == "application/json":
                self.buffer += "]".encode()
            await self.writer.write(self.buffer)
            await self.writer.close()
        elif isinstance(data, dict):
            # data is object, it is one iteration {data}

            if not hasattr(self,"buffer"):
                logger.debug("no buffer, should occur once")
                self.buffer = bytearray()
                self.chunk_count = 0
                self.send_count = 0
                await self.writer.open(self.content_type)

            if self.content_type in ["application/json", "application/x-ndjson"]:
                for key, value in data.items():
                    if isinstance(value, datetime) or isinstance(value, date):
                        data[key] = value.isoformat()
                    if key == "upl":
                        if data.get("upl", None):
                            data[key] = [
                                item[0] for item in Counter(data["upl"]).most_common()
                            ]
                if self.content_type == "application/json":
                    self.buffer += (
                        "["
                        if (self.send_count == 0 and self.chunk_count == 0)
                        else ","
                    ).encode()
                self.buffer += (jsn.dumps(data) + "\n").encode()
            elif self.content_type == "text/csv":
                if not hasattr(self,"csvwriter"):
                    bf = BufferWriter(self)
                    self.csvwriter = csv.writer(
                        bf, delimiter=";", quoting=csv.QUOTE_ALL
                    )
                    self.csvwriter.writerow(prepare_for_csv(None))
                self.csvwriter.writerow(prepare_for_csv(data))
            elif self.content_type == "text/plain":
                if len(data) == 1:
                    self.buffer += (f"{list(data.values())[0]}\n").encode()
                else:
                    for key in sorted(data.keys()):
                        self.buffer += (f"{key}: {data[key]}\n").encode()

            self.chunk_count += 1
            if self.chunk_count == self.chunk_size:
                logger.debug(
                    "ok so reached chunk size, write to response {request.ctx.response}"
                )
                await self.writer.write(self.buffer)
                self.buffer = bytearray()
                self.chunk_count = 0
                self.send_count += 1
        else:
            raise Exception(f"provided data object type ({type(data)}) not supported")
                

    async def write(self,data):
        if isinstance(data, AsyncGeneratorType):
            logger.debug("data is generator, so iterate")
            processed = False
            async for d in data:
                processed = True
                await self._write_record(d)
            if processed:
                await self._write_record(None)
            return processed
        return False
