import json
import logging
from statistics import AsyncStatistics, parse_datetime
from psycopg.rows import dict_row
from psycopg_pool import AsyncConnectionPool
import os
import uuid
from collections import Counter, OrderedDict
from datetime import datetime
from datetime import timezone as tz
from zoneinfo import ZoneInfo
from types import SimpleNamespace
from psycopg.types.json import Jsonb

logger = logging.getLogger(__name__)

# Database configuration
DB_CONFIG = {
    "host": os.getenv("DB_HOST"),
    "port": int(os.getenv("DB_PORT")),
    "user": os.getenv("DB_USERNAME"),
    "password": os.getenv("DB_PASSWORD"),
    "dbname": os.getenv("DB_DATABASE"),
    "row_factory": dict_row,
    "application_name": "statistics"
}

class AsyncPostgresStatistics(AsyncStatistics):

    def __init__(self):
        self.pool = AsyncConnectionPool(kwargs=DB_CONFIG, open=False)

    def __call__(self) -> "AsyncStatistics.Executor":
        return self.Executor(self)

    def dry_run(self) -> "AsyncStatistics.Executor":
        return self.Executor(self, dry=True)

    async def open(self):
        await self.pool.open()

    async def close(self):
        await self.pool.close()
        self.pool = None


    class Executor(AsyncStatistics.Executor):
        def __init__(self, statistics, dry=False):
            self.statistics = statistics
            self.conn = None
            self.dry = dry

        async def __aenter__(self):
            logger.debug(f"entering executor context")
            self.conn = await self.statistics.pool.getconn()
            return self

        async def __aexit__(self, exc_type,exc_value, traceback):
            logger.debug(f"closing executor {exc_type} {exc_value}\n{traceback}")

            # Cleanup connection
            if self.conn:
                if not self.dry and not exc_type:
                    await self.conn.commit()
                await self.statistics.pool.putconn(self.conn)

        async def _execute(self,sql,params):
            logger.debug(f"query {sql} with params {params}")
            async with self.conn.cursor() as acur:
                await acur.execute(sql,params)
                # Fetch one row at a time
                row_dict = await acur.fetchone()
                # first row {row_dict}
                return row_dict

        async def _query(self,sql,params):
            logger.debug(f"query {sql} with params {params}")
            async with self.conn.cursor() as acur:
                await acur.execute(sql,params)
                # Fetch one row at a time
                row_dict = await acur.fetchone()
                # first row {row_dict}
                if not row_dict:
                    return
                # entering loop...
                while row_dict:
                    yield row_dict
                    row_dict = await acur.fetchone()

        async def query_log(self, query, organisation_id = None,timezone = "Europe/Amsterdam"):
            filter_q = ""
            first = True
            cases = query.split(',')
            params = []
            for case in cases:
                if first:
                    filter_q += " WHERE "
                    first = False
                else:
                    filter_q += " AND "
                synonyms = case.split('|')
                if len(synonyms) > 1:
                    first_synonym = True
                    for synonym in synonyms:
                        if first_synonym:
                            filter_q += "("
                            first_synonym = False
                        else:
                            filter_q += " OR "
                        filter_q += "message ilike %s"
                        params.append('%'+synonym+'%')
                    filter_q += ")"
                else:
                    filter_q += "message ilike %s"
                    params.append('%'+synonyms[0]+'%')
            

            query = f"SELECT message FROM public.events_event e {filter_q}  and event_type = 'user_message' order by timestamp"
            print (params)
            print (query)
            return self._query(query,params)
        
        async def query_statistics(self, timezone = "Europe/Amsterdam",**kwargs):
            filter_q = ""
            first = True
            params = kwargs
            for k, v in params.items():
                if first:
                    filter_q += " WHERE "
                    first = False
                else:
                    filter_q += " AND "
                if k == "start__gte":
                    params[k] = parse_datetime(v)
                    filter_q += f"s.start > %(start__gte)s"
                elif k == "start__lt":
                    params[k] = parse_datetime(v)
                    filter_q += f"s.start < %(start__lt)s"
                elif k == "session_id" and isinstance(v, list):
                    filter_q += f"s.session_id = ANY(%(session_id)s)"
                else:
                    filter_q += f"s.{k} = %({k})s"

            params['timezone'] = timezone

            fields = '''session_id, 
                organisation_id,
                "start" AT TIME ZONE %(timezone)s as "start",
                "end" AT TIME ZONE %(timezone)s as "end",
                DATE("start" AT TIME ZONE %(timezone)s) as "date",
                EXTRACT(HOUR FROM "start" AT TIME ZONE %(timezone)s)::int as "hour",
                EXTRACT(ISOYEAR FROM "start" AT TIME ZONE %(timezone)s)::int as "iso_weekdate_year", 
                EXTRACT(MONTH FROM "start" AT TIME ZONE %(timezone)s)::int as "month", 
                EXTRACT(WEEK FROM "start" AT TIME ZONE %(timezone)s)::int as "week", 
                EXTRACT(ISODOW FROM "start" AT TIME ZONE %(timezone)s)::int as "weekday", 
                EXTRACT(YEAR FROM "start" AT TIME ZONE %(timezone)s)::int as "year", 
                "source",
                "escalate", "escalate_time","escalate_result","escalate_result_time", 
                nullif((EXTRACT(EPOCH FROM(escalate_result_time-escalate_time))::int),0) as "escalate_wait", 
                "feedback", "feedback_result","upl","performance","is_evaluated","critical_error",
                "different_language","num_correct_answers","num_correct_answers_after_one_correction","num_correct_answers_after_two_corrections", "num_failed_corrections", "num_impossible_questions",
                "num_incorrect_responses","citizen_properly_assisted","personal_question", "subjects", "classification","remarks"'''

            query = f"SELECT {fields} FROM public.statistics_conversationstatistics s {filter_q} order by start"

            return self._query(query,params)

        async def update_statistics(self, organisation_id, session_id, timezone = "Europe/Amsterdam", **kwargs):
            accepted_fields = ['start', 'end', 'source', 'upl', 'performance', 'escalate', 'escalate_result', 'escalate_time', 'escalate_result_time', 'escalate_wait', 'feedback', 'feedback_result', 'is_evaluated','subjects','classification','remarks']

            insert_dict = { # db schema currently contains not nullable fields, initialize those fields correctly / backward compatibility / future deprecation
                'organisation_id': organisation_id,
                'session_id': session_id,
                'source': "",
                'escalate': "",
                'escalate_result': "",
                'feedback': "",
                'feedback_result': "",
                'upl': [],
                'start': datetime.now(tz.utc),
                'end':datetime.now(tz.utc),
                'performance': Jsonb({}),
                'critical_error': False,
                'different_language': False,
                'is_evaluated': False,
                'personal_question': False,
                'remarks': "",
                'subjects': []
            }

            update_dict = {key: Jsonb(kwargs[key]) if isinstance(kwargs[key],dict) else kwargs[key] for key in accepted_fields if key in kwargs}

            calc_dict = update_dict if 'start' in update_dict else insert_dict #determine on what dictionary to process calculations

            start = calc_dict['start']
            local_dt = start.astimezone(ZoneInfo(timezone))
            naivestart = local_dt.replace(tzinfo=None)
            calc_dict['date'] = naivestart
            calc_dict['hour'] = naivestart.hour
            calc_dict['iso_weekdate_year'] = naivestart.isocalendar()[0]
            calc_dict['month'] = naivestart.month
            calc_dict['week'] = naivestart.isocalendar()[1]
            calc_dict['weekday'] = naivestart.weekday()+1
            calc_dict['year'] = naivestart.year

            insert_dict.update(update_dict) # always update insert dict with provided and accepted key / value pairs = update_dict

            sql_insert = '("organisation_id", "session_id"'
            sql_insert_values = "(%(organisation_id)s, %(session_id)s"
            for field in insert_dict.keys():
                if field in ['organisation_id', 'session_id']:
                    continue
                sql_insert += f', "{field}"'
                sql_insert_values += f", %({field})s"
            sql_insert += ")"
            sql_insert_values += ")"

            sql_update = ""
            first_field = True
            for field in update_dict.keys():
                if field in ['organisation_id', 'session_id']:
                    continue
                if first_field:
                    first_field = False
                else:
                    sql_update += ", "
                # sql_update += f'"{field}" = %({field})s'
                sql_update += f'"{field}" = EXCLUDED.{field}'
            # sql_update += f',"personal_question" = True' # test update statement applied

            sql_returning = '*'
            upsert_query = (f'''
                INSERT INTO public.statistics_conversationstatistics {sql_insert}
                VALUES {sql_insert_values}
                ON CONFLICT (session_id)
                DO UPDATE SET {sql_update}
                RETURNING {sql_returning}
            ''')
            logger.debug(f"executing query:\n{upsert_query}\nwith parameters: {insert_dict}")
            return await self._execute(upsert_query,insert_dict)
            # async for r in self._query(upsert_query,insert_dict):
            #     logger.debug(f"result: {r}")
            #     yield r
            # async with self.conn.cursor() as acur:
            #     await acur.execute(upsert_query, data)

        def _process_statistics_wrapup_conversation(self, metadata, firstusermessagedata, latestevent, upldata, performance, escalationdata, feedbackdata):
            latesteventdata = {'end':latestevent['timestamp']}

            # Initialize placeholders for matches
            other_match = None
            unknown_match = None
            success_match = None
            
            for item in escalationdata:
                if item.get("escalate_result") == "success":
                    success_match = item  # Return immediately if "success" is found
                elif item.get("escalate_result") == "unknown" and unknown_match is None:
                    unknown_match = item  # Save the first "unknown" match
                elif other_match is None:
                    other_match = item  # Save the first item (fallback)
            if not other_match:
                other_match = {}
            
            # Return "unknown" match if found, otherwise the first item
            escalatedata = success_match if success_match else unknown_match if unknown_match else other_match

            statistics = metadata | firstusermessagedata | latesteventdata | {'upl':upldata,'performance':dict(performance | {"total":performance.total()})} | feedbackdata | escalatedata
            return statistics

        async def _process_statistics(self, data):
            metadata = None
            firstusermessagedata = None
            latestevent = None
            upldata = None
            performance = None
            escalationdata = None
            feedbackdata = None

            usereventcount = 0
            c_session_id = None
            async for event in data:
                if event['session_id'] != c_session_id: # session_id has changed, first wrap up session and emit it
                    if c_session_id and usereventcount > 1: # only when it exists offcourse and if there were more that 1 user event
                        yield self._process_statistics_wrapup_conversation(metadata,firstusermessagedata,latestevent,upldata,performance,escalationdata,feedbackdata)

                    metadata = {'organisation_id':event['organisation_id'],'session_id':event['session_id']}
                    firstusermessagedata = None
                    usereventcount = 0
                    latestevent = None
                    upldata = []
                    performance = Counter()
                    escalationdata = []
                    feedbackdata = {}
                    c_session_id = event['session_id']
                latestevent = event
                if not event['agent_type']: # if agent type is blank, then it's a user message
                    if not firstusermessagedata:
                        firstusermessagedata = {}
                        customData = event['incoming_data']['customData']
                        firstusermessagedata['source'] = customData.get("chatStartedOnPage",None)
                        firstusermessagedata['start'] = event['timestamp']
                    usereventcount += 1 # count user events, should count from first extra entry TODO: what about whatsapp in future, this depends on /greet and /direct_livechat
                else: #responses from agents (not even 'bot' persé)
                    timestamp = event['timestamp']
                    statistics = event['incoming_data'].get('metadata',{'statistics':{}}).get('statistics',{})
                    if 'upl' in statistics:
                        upldata.append(statistics['upl'])
                    if 'performance' in statistics:
                        pv = statistics['performance']
                        performance[pv] += 1
                        performance["total"] += 1
                    if 'feedback' in statistics:
                        feedbackdata['feedback'] = statistics['feedback']
                    if 'feedback_result' in statistics:
                        feedbackdata['feedback_result'] = statistics['feedback_result']
                    if 'escalate' in statistics:
                        escalate = {'escalate':statistics['escalate'],'escalate_result':'unknown','escalate_time':timestamp,'escalate_result_time':None}
                        escalationdata.append(escalate)
                    if 'escalate_result' in statistics:
                        if not escalationdata:
                            escalate = {'escalate':'unknown','escalate_time':None}
                        else:
                            escalate = escalationdata[-1]
                        escalate['escalate_result_time'] = timestamp
                        escalate['escalate_result'] = statistics['escalate_result']

            if c_session_id and usereventcount > 1: # only when it exists offcourse and if there were more that 1 user event
                yield self._process_statistics_wrapup_conversation(metadata,firstusermessagedata,latestevent,upldata,performance,escalationdata,feedbackdata)

        async def generate_statistics(self, organisation_id = None, session_id = None, start = None, test = False, timezone = "Europe/Amsterdam"):
            # organisation_id = "Meierijstad"
            # session_id = ['0492520f-e6a9-4177-bea0-fdf43e4dbc67','1a5a25b4-bf8f-40f6-adf6-1145b5e068a2','1e818e93-d1f9-4ed6-84a6-654114468aa5','299d956c-cd60-4773-8681-69f43a659163']
            session_filter = ""
            if isinstance(session_id, list):
                session_filter = "e.session_id = ANY(%(session_id)s)"
            elif isinstance(session_id, str):
                session_filter = "e.session_id = %(session_id)s"
            else:
                session_filter = "e.session_id IN (SELECT session_id FROM first_events)"

            pre_query = ""
            date_filter = ""
            if start:
                date_filter = " AND e.timestamp > %(start)s"
            else:
                date_filter = " AND s.session_id IS NULL"
            test_filter = ""
            if isinstance(test,str):
                test = test.lower()  # Convert to lowercase for case-insensitivity
                if test in ("yes", "true", "t", "1", "staging"):
                    test = True
                # elif test in ("no", "false", "f", "0"):
                #     test = False
                else:
                    test = False
            if test:
                test_filter = " AND e.test_session"
            else:
                test_filter = " AND NOT e.test_session"

            if not session_id:
                pre_query = (f'''
                    WITH first_events AS (
                        SELECT DISTINCT ON (session_id) e.session_id, e.timestamp
                        FROM public.events_event e
                        LEFT JOIN public.statistics_conversationstatistics s ON e.session_id = s.session_id
                        WHERE e.timestamp < CURRENT_TIMESTAMP - interval '1 hours'{test_filter}{date_filter}
                        ORDER BY session_id, timestamp ASC
                    )
                ''')
            organisation_filter = ""
            if organisation_id:
                organisation_filter = " AND e.organisation_id = %(organisation_id)s"

            params = {'timezone': timezone,'organisation_id':organisation_id,'session_id':session_id,'start':parse_datetime(start) if start else None}
            query = (f'''
                {pre_query}
                SELECT e.session_id, e.timestamp, e.organisation_id, e.agent_type, e.source, e.channel, e.test_session, e.incoming_data
                FROM public.events_event e
                WHERE {session_filter}{organisation_filter}
                ORDER BY e.session_id, e.timestamp
            ''')

            #TODO e.test_session

            async for s in self._process_statistics(self._query(query,params)):
                returned = await self.update_statistics(**s)
                yield returned



        # async def generate_statistics(self, until = None, timezone = "Europe/Amsterdam"):
        #     untilpart = ""
        #     params = {}
        #     if until:
        #         untilpart = " AND %(until)s > e.timestamp"
        #         params['until'] = until
        #     params['timezone'] = timezone
        #     query = (f'''
        #         SELECT DISTINCT ON (e.session_id) e.session_id, e.timestamp AT TIME ZONE %(timezone)s as timestamp, e.organisation_id, e.message, e.source, e.channel, e.test_session
        #         FROM public.events_event e
        #         LEFT JOIN public.statistics_conversationstatistics s ON e.session_id = s.session_id
        #         WHERE s.session_id IS NULL AND e.test_session = true{untilpart}
        #         ORDER BY e.session_id, e.timestamp ASC
        #     ''')
        #     return self._query(query,params)
            


# TODO: FUTURE AGGREGATION SQL BUILDER

# def _build_sql_group(field):
#     field_info = FIELD_INFO.get(
#         field, {"aggregate": None, "groupable": False, "filter": False}
#     )
#     if not field_info["groupable"]:
#         return None
#     else:
#         return f"s.{field}"


# def _build_sql_aggregate(field):
#     field_info = FIELD_INFO.get(
#         field, {"aggregate": None, "groupable": False, "filter": False}
#     )

#     if field_info["aggregate"] == "count":
#         return f"count(s.{field})"
#     elif field_info["aggregate"] == "sum":
#         return f"sum(s.{field})"
#     elif field_info["aggregate"] == "sum_bool":
#         return f"sum(s.{field}::int)"
#     elif field_info["aggregate"] == "performance":
#         return (
#             "jsonb_build_object("
#             f"'total', SUM(({field}->>'total')::INTEGER),"
#             f"'reparation', SUM(({field}->>'reparation')::INTEGER),"
#             f"'misunderstanding', SUM(({field}->>'misunderstanding')::INTEGER),"
#             f"'chitchat', SUM(({field}->>'chitchat')::INTEGER),"
#             f"'direct', SUM(({field}->>'direct')::INTEGER),"
#             f"'indirect', SUM(({field}->>'indirect')::INTEGER),"
#             f"'unsupported', SUM(({field}->>'unsupported')::INTEGER)"
#             ")"
#         )
#     elif field_info["aggregate"] == "percentage_list":
#         return f"string_agg(array_to_string(s.{field},','),',')"
#     elif field_info["aggregate"] == "percentage":
#         return f"string_agg(s.{field},',')"
#     elif field_info["aggregate"] == "wait":
#         return f"array_agg(ceil(s.{field}::NUMERIC / 5) * 5)"

#     return None


# def build_sql(group_by_x=[], group_by_y=[]):
#     aggregate_fields = FIELD_INFO.copy()
#     query = "SELECT "
#     for f in group_by_y:
#         aggregate_fields.pop(f)
#         q_field = _build_sql_group(f)
#         if q_field:
#             query += q_field + f" as {f}, "
#     for f in group_by_x:
#         aggregate_fields.pop(f)
#         q_field = _build_sql_group(f)
#         if q_field:
#             query += q_field + f" as {f}, "
#     for f in aggregate_fields.keys():
#         q_field = _build_sql_aggregate(f)
#         if q_field:
#             query += q_field + f" as {f}, "
#     query = query[:-2] + " "
#     query += "FROM public.statistics_conversationstatistics s"
#     query += "WHERE s.organisation_id = 'Utrecht' and s.start > '2024-01-01' "
#     if len(group_by_x) + len(group_by_y) > 0:
#         query_part = ""
#         for f in group_by_y:
#             q_field = _build_sql_group(f)
#             if q_field:
#                 query_part += q_field + ", "
#         for f in group_by_x:
#             q_field = _build_sql_group(f)
#             if q_field:
#                 query_part += q_field + ", "
#         query_part = query_part[:-2] + " "
#         query += "GROUP BY " + query_part
#         query += "ORDER BY " + query_part
#     return query
