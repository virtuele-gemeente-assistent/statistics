#!/usr/local/bin/python
import asyncio
import logging
import aiohttp
import itertools
import copy
import sys
from aiohttp import web
import os
from datetime import timezone
import datetime
from apiv1 import apiv1
import commands

import requests
from elasticapm.contrib.sanic import ElasticAPM
from sanic import Blueprint, Sanic, response, exceptions
from sanic.request import Request
from sanic.response import HTTPResponse, json, text
import jwt
import argparse



# Read the log level from the environment variable (LOG_LEVEL)
log_level = os.getenv("LOG_LEVEL")

if log_level:
    log_level = log_level.upper()

# Map the environment variable to a logging level
log_level_mapping = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL
}

# Set the log level from the environment variable or default
logging.basicConfig(level=log_level_mapping.get(log_level, logging.INFO))

logger = logging.getLogger(__name__)


# if os.getenv("ELASTIC_APM_SERVER_URL"):
#     environment = os.getenv("ENVIRONMENT")
#     app.config.update({
#         "ELASTIC_APM_SERVER_URL": os.getenv("ELASTIC_APM_SERVER_URL"),
#         "ELASTIC_APM_SECRET_TOKEN": os.getenv("ELASTIC_APM_SECRET_TOKEN"),
#     })
#     apm = ElasticAPM(app=app, config={"SERVICE_NAME": f"Environment {environment}"})
app = Sanic(name="statistics")
app.config.SECRET = "KEEP_IT_SECRET_KEEP_IT_SAFE"
app.blueprint(apiv1)

sentry_dsn = os.getenv("STATISTICS_SENTRY_DSN", None)
if sentry_dsn:
    import sentry_sdk
    from sentry_sdk.integrations.sanic import SanicIntegration
    from sentry_sdk.integrations.redis import RedisIntegration

    sentry_sdk.init(dsn=sentry_dsn, integrations=[SanicIntegration(), RedisIntegration()])

@app.get("/login")
async def do_login(request):

    token = {'sub':'test@test.nl','aud':'statistics','exp':datetime.datetime.now(tz=timezone.utc) + datetime.timedelta(seconds=3600)}
    if 'sub' in request.args:
        token['sub'] = request.args['sub'][0]
    # if 'uname' in request.args:
    #     token['uname'] = request.args['uname'][0]
    if 'org' in request.args:
        token['org'] = request.args['org'][0]
    enc = jwt.encode(token, request.app.config.SECRET)
    token['token'] = enc
    token.pop('exp')
    return json(token)

@app.route("/", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    # await sio.emit("bot_uttered", {"text": data["message"]}, room=sid)
    return response.json({"status": "ok"})

if __name__ == "__main__":
    arguments = sys.argv
    parser = argparse.ArgumentParser(
                        prog='conversations',
                        description='Service for storing and accessing conversations and statistics',
                        epilog='Licensed under the EUPL 1.2')
    parser.add_argument('command', type=str, help='command to execute')      # option that takes a value ,choices=['generate-statistics']
    parser.add_argument('--dry-run', action='store_true', help='dry run, do not make changes')      # option that takes a value ,choices=['generate-statistics']

    args, unknown = parser.parse_known_args()

    params = {}
    paramstring = None
    for arg in unknown:
        kv = arg.split('=')
        if len(kv) == 2:
            params[kv[0]] = kv[1]
            if paramstring:
                paramstring += ", "
            else:
                paramstring = ""
            paramstring += kv[0]+'='+kv[1]

    if args.command == "serve":
        logger.info("Starting daemon / application server")
        app.run(host="0.0.0.0")
    else:
        command = args.command
        logger.info(f"Executing command {command} with arguments {args} and extra parameters {paramstring}")
        asyncio.run(commands.run_command(command,args.dry_run,**params))


    
