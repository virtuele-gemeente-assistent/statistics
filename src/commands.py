import json
import logging
import os
from collections import Counter, OrderedDict
from datetime import datetime, date, timezone
from statistics import AsyncStatistics
from outputwriter import AsyncBufferedWriter, AsyncBufferedFormatter
from types import AsyncGeneratorType
import inspect


logger = logging.getLogger(__name__)

class AsyncBufferedPrintWriter(AsyncBufferedWriter):
    def __init__(self):
        super().__init__()

    async def open(self, content_type):
        pass

    async def write(self,data):
        print(data.decode())

    async def close(self):
        pass

def convert_dates_to_iso(data):
    for key, value in data.items():
        if isinstance(value, datetime):  # Check if the value is a datetime object
            data[key] = value.isoformat()
    return data


async def run_command(command,dry_run=False,**kwargs):
    logger.debug(f"command: {command}, arguments: {kwargs}")
    statistics = AsyncStatistics.get_instance()
    await statistics.open()
    ctx = statistics.dry_run() if dry_run else statistics()
    async with ctx as executor:
        method = getattr(executor,command)
        if inspect.iscoroutinefunction(method):
            result = await method(**kwargs)
        else:
            result = method(**kwargs)
        if isinstance(result, AsyncGeneratorType):
            writer = AsyncBufferedPrintWriter()
            formatter = AsyncBufferedFormatter(writer)
            if not await formatter.write(result):
                print("no result from query")
        elif isinstance(result,dict):
            print(json.dumps(convert_dates_to_iso(result)))
        elif isinstance(result,str):
            print(result)
        else:
            print(f"output of type {type(result)} not yet supported")
    await statistics.close()

