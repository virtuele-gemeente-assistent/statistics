import json
import logging
from abc import ABC, abstractmethod
import os
from collections import Counter, OrderedDict
from datetime import datetime, date, timezone
from dateutil.parser import parse
from zoneinfo import ZoneInfo

logger = logging.getLogger(__name__)

def parse_datetime(dt_str):
    naive_datetime = parse(dt_str)
    amsterdam_tz = ZoneInfo("Europe/Amsterdam")
    aware_datetime = naive_datetime.replace(tzinfo=amsterdam_tz)
    return aware_datetime

class AsyncStatistics(ABC):

    _instance = None

    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            if "DB_DATABASE" in os.environ:
                from statistics_psycopg import AsyncPostgresStatistics
                cls._instance = AsyncPostgresStatistics()
            else:
                cls._instance = None
        return cls._instance
    
    @abstractmethod
    async def open(self):
        pass

    @abstractmethod
    def __call__(self) -> "AsyncStatistics.Executor":
        pass

    @abstractmethod
    def dry_run(self) -> "AsyncStatistics.Executor":
        pass

    @abstractmethod
    async def open(self):
        pass
    
    @abstractmethod
    async def close(self):
        pass

    class Executor(ABC):

        @abstractmethod
        async def __aenter__(self) -> "AsyncStatistics.Executor":
            pass

        @abstractmethod
        async def __aexit__(self, exc_type,exc_value, traceback):
            pass

        @abstractmethod
        async def query_statistics(self,  timezone = "Europe/Amsterdam",**kwargs):
            # should return a async generator that yields dictionaries
            pass

        @abstractmethod
        async def query_log(self, query, organisation_id = None,timezone = "Europe/Amsterdam"):
            # should return a async generator that yields dictionaries
            pass

        @abstractmethod
        async def update_statistics(self, organisation_id, session_id, timezone = "Europe/Amsterdam", **kwargs):
            pass

        @abstractmethod
        async def generate_statistics(self, organisation_id = None, session_id = None, start = None, test = False, timezone = "Europe/Amsterdam"):
            pass
